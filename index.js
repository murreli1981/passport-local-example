const express = require("express");
const passport = require("passport");
const { Strategy } = require("passport-local");
const session = require("express-session");

const app = express();
const port = 3000;

app.use(express.json());

//Session middleware
const sessionMiddleware = session({
  secret: "fruta",
  resave: true,
  saveUninitialized: true,
  rolling: true,
  cookie: {
    maxAge: 20 * 1000,
  },
});

passport.use(
  new Strategy("local", (username, password, done) => {
    const userOk = "coder";
    const passOk = "123456";
    if (username === userOk && password === passOk) {
      return done(null, { username, password });
    }

    return done(null, false);
  })
);

passport.serializeUser(function (user, done) {
  done(null, user);
});

passport.deserializeUser(function (user, done) {
  done(null, user);
});

app.use(sessionMiddleware);
app.use(passport.initialize());
app.use(passport.session());

app.get("/", (req, res) => {
  res.send("Hello World!");
});

app.post(
  "/login",
  passport.authenticate("local", {
    //failureRedirect: "/loginFailed",
  }),
  (req, res, next) => {
    const { username, password } = req.body;
    res.send({ username, password });
  }
);

app.get("/loginFailed", (req, res, next) => {
  res.send("failed");
});

app.listen(port, () => {
  console.log(`server listening at http://localhost:${port}`);
});
